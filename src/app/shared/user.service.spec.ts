import { TestBed } from '@angular/core/testing';
import {HttpTestingController, HttpClientTestingModule} from '@angular/common/http/testing'
import { UserService } from './user.service';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms/'
describe('UserService', () => {
  let service: UserService;
  let httpMock : HttpTestingController;
  let fb : FormBuilder;
  const dummyTokenData = {
    clientId: 3,
    username: "starjohnson",
    password: "password",
    firstName: "Star",
    lastName: "Johnson",
    address: "Space",
    age: 55,
    email: "starjohnson@maildrop.cc",
    contactNumber: "5015558888",
    photoUrl: null,
    accList: [
        {
            accountNumber: "132548101",
            balance: 5005.02,
            transList: [
                {
                    transactionId: 4,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 0.0,
                    creditAmount: 5000.02,
                    currentBalance: 5005.02,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 1,
                typeName: "checking"
            },
            bholder: {
                branchId: 1,
                branchName: "Santa Clara"
            }
        },
        {
            accountNumber: "132548102",
            balance: 4.999519998E7,
            transList: [
                {
                   transactionId: 1,
                   description: "Payday",
                   TransactionDateTime: "2021-01-28, 23:41",
                   debitAmount: 0.0,
                   creditAmount: 200.0,
                   currentBalance: 5.00002E7,
                   transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 2,
                    description: "Purchase of Amazing Figure",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 280.0,
                    creditAmount: 0.0,
                    currentBalance: 4.999992E7,
                    transactionDateTime: "2021-01-28, 23:41"
                },
                {
                    transactionId: 3,
                    description: "Transfer of money from savings to checking",
                    TransactionDateTime: "2021-01-28, 23:41",
                    debitAmount: 5000.02,
                    creditAmount: 0.0,
                    currentBalance: 4.999519998E7,
                    transactionDateTime: "2021-01-28, 23:41"
                }
            ],
            tholder: {
                typeId: 2,
                typeName: "savings"
            },
            bholder: {
                branchId: 3,
                branchName: "San Francisco"
            }
        }
    ]

};
const dummyStatementData = {
  accountNumber: "132548101",
  balance: 5005.02,
  transList: [
      {
          transactionId: 4,
          description: "Transfer of money from savings to checking",
          TransactionDateTime: "2021-01-28, 23:41",
          debitAmount: 0.0,
          creditAmount: 5000.02,
          currentBalance: 5005.02,
          transactionDateTime: "2021-01-28, 23:41"
      }
  ],
  tholder: {
      typeId: 1,
      typeName: "checking"
  },
  bholder: {
      branchId: 1,
      branchName: "Santa Clara"
  }
};
const dummyLoginData = {
  username: 'starjohnson',
  password: 'password'
};

const dummyNewAccountData = {
  typeId: '1',
  branchId: '1',
  clientId:'3'
};

const dummyStatement = {
 
};

const dummyEmailData = {
  email: "starjohnson@maildrop.cc"
}
const dummyRegisterData =   {
  username: "john",
  firstName: "john",
  lastName: "lao",
  address:"Santa Paula",
  age: "10",
  emailAddress: "laojohnmatthew@gmail.com",
  contactNumber: "2246237364",
  password:"mypassword",
  photoUrl: null
}
  beforeEach(() => {
    TestBed.configureTestingModule({

      imports: [HttpClientTestingModule, ReactiveFormsModule, FormsModule],
      providers: [UserService],

    });
    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
    fb = TestBed.inject(FormBuilder);
  });
let dummyDeleteAccount = {
  accountNumber: "132548101"
};
  afterEach(()=>{
    httpMock.verify();
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should retrieve the token on good login', ()=>
  {
    service.login(dummyLoginData).subscribe((res) =>{
      expect(res.toString()).toEqual(dummyLoginData.toString());
    });
    const req = httpMock.expectOne('http://localhost:9030/api/users/login');
    expect(req.request.method).toBe('POST');
    req.flush(dummyTokenData);
  });
  // it('should call forgotPassword ', () =>{
  //   service.forgotPassword(dummyEmailData).subscribe((res) =>{
  //     expect(res.toString()).toEqual(dummyLoginData.toString());
  //   });
  //   const req = httpMock.expectOne('http://localhost:9030/api/users/forgotpassword');
  //   expect(req.request.method).toBe('POST');
  //   req.flush(dummyTokenData);

  // });
  // it('should call fundtransfer ', () =>{
  //   service.fundtransfer("132548101", "132548102", "500.02").subscribe((res) =>{
  //     expect(res.toString()).toEqual(dummyLoginData.toString());
  //   });
  //   const req = httpMock.expectOne('http://localhost:9030/api/bank/transfer');
  //   expect(req.request.method).toBe('POST');
  //   req.flush(dummyTokenData);

  // });


 it('should call regiser', ()=>{
    service.register("file").subscribe((res) => {
      expect(res.toString()).toEqual(JSON.stringify(dummyRegisterData));
    })

    const req = httpMock.expectOne('http://localhost:9030/api/users/register');
    expect(req.request.method).toBe('POST');
    req.flush(dummyRegisterData);

    //   service.createNewAccount().subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyStatementData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/bank/profile/createaccount');
//   expect(req.request.method).toBe('POST');
//   req.flush(dummyStatementData);
 });

//  it('should call getStatement()', ()=>{
//   service.getStatement("132548101").subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyStatementData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/transaction/view?account=132548101');
//   expect(req.request.method).toBe('GET');
//   req.flush(dummyStatementData);

//  });
//  it('should call updateProfile()', ()=>{
//   service.updateProfile().subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyStatementData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/bank/profile/updateprofile');
//   expect(req.request.method).toBe('PUT');
//   req.flush(dummyStatementData);

//  });
//  it('should call deleteAccount()', ()=>{
//   service.deleteAccount("132548101").subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyStatementData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/transaction/delete');
//   expect(req.request.method).toBe('POST');
//   req.flush(dummyStatementData);

//  });
//  it('should call createNewAccount', ()=>{
//   service.createNewAccount().subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyStatementData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/bank/profile/createaccount');
//   expect(req.request.method).toBe('POST');
//   req.flush(dummyStatementData);

//  });


//  it('should call updateToken()', ()=>{
//   service.updateToken().subscribe((res) =>{
//     expect(res.toString()).toEqual(dummyTokenData.toString());
//   });
//   const req = httpMock.expectOne('http://localhost:9030/api/users/updateToken');
//   expect(req.request.method).toBe('POST');
//   req.flush(dummyTokenData);

//  });

});
