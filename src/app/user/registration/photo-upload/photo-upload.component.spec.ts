import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { PhotoUploadComponent } from './photo-upload.component';
describe('PhotoUploadComponent', () => {
  let component: PhotoUploadComponent;
  let fixture: ComponentFixture<PhotoUploadComponent>;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PhotoUploadComponent ],
      imports: [HttpClientTestingModule],
      providers: [FormBuilder]
    })
    .compileComponents();
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(PhotoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call onUploadFile when input is changed for required files', () => {
    spyOn(component, 'upload').and.callThrough();
    const fakeChangeEvent = new Event('change');
    const targ = fixture.nativeElement.querySelector('input#uploadFile');
    targ.dispatchEvent(fakeChangeEvent);
    fixture.whenStable().then(() => {
      expect(component.upload).toHaveBeenCalled();
      expect(targ.files.length).toBeGreaterThan(0);
    });
  });
});
