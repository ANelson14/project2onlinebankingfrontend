import { Component, OnInit } from '@angular/core';
import { FormBuilder, NgForm, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPassForm = {
    email:'' 
  }
  constructor(public service: UserService) { }

  getPass(form: NgForm) {
    this.service.forgotPassword(form.value).subscribe(
      response => {
        
      }
    )
  }
  ngOnInit(): void {

  }

}
