import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import { BankComponent } from './bank.component';
import { Location } from '@angular/common';
import { Form, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../shared/user.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';


describe('BankComponent', () => {
  let component: BankComponent;
  let fixture: ComponentFixture<BankComponent>;
  let router: Router;
  let fb: FormBuilder;
  let service; 
  let http: HttpTestingController;
  beforeEach(async () => {
     TestBed.configureTestingModule({
      declarations: [ BankComponent ],
      imports: [RouterTestingModule.withRoutes([]), ReactiveFormsModule, HttpClientTestingModule],
      providers: [UserService,]
     })
    .compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(BankComponent);
    service = TestBed.inject(UserService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call onlogout', ()=> {
    const logout = fixture.debugElement.nativeElement.querySelector('a');
    logout.click();
    fixture.detectChanges();
    router = TestBed.get(Router);

    let location:Location = TestBed.get(Location);
    router.navigate(['./logout']).then( ()=>{
      expect(location.path()).toBe('./login');
    });
  });
});
