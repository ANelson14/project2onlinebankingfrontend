import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {
  client!:Client;
 
  
  constructor(private router:Router, public service: UserService) { }

  ngOnInit(): void {
    this.service.updateToken().subscribe(res =>{
       localStorage.setItem('token', null);
      // console.log(this.client);
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
    this.client =  JSON.parse(localStorage.getItem('token'));

  }
  
}
